# IPv6 only hackathon radio

creates a radio based on a playlist where anonymous people with the link can alter it uploading and deleting its current content

3:40 AM (not Summer Time) instructions:

- nextcloud: where users update playlist in a loginless way just with the link
  - install nextcloud [following a guide](https://gitlab.com/guifi-exo/wiki/-/blob/master/howto/nextcloud.md#install-nextcloud-in-debian10-nginx-and-postgresql)
    - it is recommended to use postgresql and nginx
  - enable app: "External storage support"
  - create a dir named playlist that points to `/var/www/playlist` and make it shareable through the "External storage support" app
  - go to the playlist and share it with *Allow upload and editing*, share it with the victims
- icecast: streams the audio
  - `apt install icecast2`
  - `dpkg-reconfigure icecast2` change the default password `hackme` with something like `pwgen -v 15` in the three places (source, relay, admin) and save that value
  - optional: if you want HTTPS, [do an nginx reverse proxy](https://gitlab.com/guifi-exo/wiki/-/blob/master/howto/nginx.md#site-template) to the `http://your_url:8000`
  - ensure you are only listening ipv6: failed! Looks like liquidsoap wants IPv4, when #1 is solved add in `/etc/icecast/icecast.xml` `<bind-address>::1</bind-address>`, but in the meantime add `<bind-address>127.0.0.1</bind-address>`
- liquidsoap
  - install liquidsoap and utilities: `apt install liquidsoap git`
  - add a user such as `adduser radio`, add it to sudo `adduser radio sudo`
  - login to it `su -l radio`
  - save the icecast secret in `/home/radio/secrets.liq` with `def icecast_passwd = "your_fantastic_password_you_generated_in_the_previous_icecast_section" end`
  - clone repo `git clone https://github.com/savonet/liquidsoap-daemon`
  - enter it `cd liquidsoap-daemon`
  - create following dir `mkdir script`
  - create following file `touch script/main.liq`
  - create main.liq script `./daemonize-liquidsoap.sh main`
  - create playlist.liq script `./daemonize-liquidsoap.sh playlist`
  - fill main and playlist with the content of this project
  - start main radio thing `sudo systemctl start main-liquidsoap.service`
    - avoid restarting main radio to avoid problems from cache browsers
  - start playlist radio thing `sudo systemctl start playlist-liquidsoap.service`
    - this is intended to be restarted, people is in main player so continuity is not going to be broken `sudo systemctl restart playlist-liquidsoap.service`
- nginx
  - ensure you are only listening ipv6 :D
